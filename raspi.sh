#!bin/bash
sudo apt update
sudo apt --no-install-recommends install ca-certificates curl python3 python3-distutils -y
sudo apt --no-install-recommends install ca-certificates curl python3 python3-dev libcurl4-openssl-dev gcc libssl-dev -y
curl -sSfO 'https://bootstrap.pypa.io/get-pip.py'
sudo python3 get-pip.py
printf '%b' '[global]\nextra-index-url=https://www.piwheels.org/simple/\n' | sudo tee /etc/pip.conf > /dev/null
sudo python3 -m pip install 'https://github.com/motioneye-project/motioneye/archive/dev.tar.gz'
sudo motioneye_init
sudo apt install dnsmasq -y

echo interface=eth0 >> etc/dnsmasq.conf
echo bind-dynamic >> etc/dnsmasq.conf
echo domain-needed >> etc/dnsmasq.conf
echo bogus-priv >> etc/dnsmasq.conf
echo dhcp-range=192.168.20.100,192.168.20.254,255.255.255.0,12h >> etc/dnsmasq.conf

sudo systemctl restart dnsmasq

sudo cp /etc/network/interfaces /etc/network/interfaces.bak

sudo cat >> /etc/network/interfaces << EOF

auto eth0
iface eth0 inet static
    address 192.168.20.99/24
EOF