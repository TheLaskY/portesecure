# PorteSecure

## Description ##
Le projet sur lequel nous avons travaillés est la réalisation d’une porte sécurisée. 
Celle-ci comporte plusieurs points. Tout d’abord un service de vidéosurveillance. 
Une caméra est positionnée au-dessus de la porte et filme en permanence permettant de surveiller l’accès à la porte, 
lorsque celle-ci détecte une personne s’approchant, la lumière s’allume et un son est émis afin de signaler la présence d’un individu.  
La deuxième fonction de la porte est le verrouillage par mot de passe. 
Pour ceci nous utilisons un joystick sur le devant de la porte, sur lequel doit être entré une certaine séquence afin de déverrouiller le loquet de la porte en émettant un son.


## Architecture Technique ##

## Points bloquants + Astuces ##
Nous avons eu des soucis de connexions entre les différents composants car nous n'avions pas de serveurs DHCP, nous les avons donc mis en adresse ip statique. 

Le code d'ouverture de la porte au Joystick viens de la culture populaire et fait référence au fameux Konami code.

Un petit moteur sert a bloquer la porte avec un loquet, celui-ci s'active en fonction des inputs effectyué sur le joystick


## Le projet ##

### Prerequis ###

Le réseau est configuré en 192.168.20.0/24
Le raspberry pi est configuré en 192.168.20.99/24 après l'execution du script
### Matériel physique ###

* raspberry pi
* ESP32-CAM + ESP32-CAM-MB
* routeur wifi
* (optionel) un switch
* windows 10
* cables RJ45

## Quick start

* cloner le projet
* Brancher le ESP32-CAM sur le ESP32-CAM-MB 
* Brancher le cable usb entre windows et ESP32-CAM
* Allumer le routeur wifi et configurer un réseau wifi en 2.4 GHz
* Avoir installé raspbian sur le raspberry pi
### Configuration raspberry pi ###

* Se connecter sur le raspbeery pi via ssh
* Executer la commande suivante: ` curl -sSL https://gitlab.com/TheLaskY/portesecure/raspi.sh | sudo bash`

### Configurer le ESP32 ###

* Brancher le ESP32 sur windows
* Executer le fichier ESP-Flasher.exe
* Selectionner le port usb ou est branché l'ESP32
* Selectionner le fichier tasmota32-webcam.bin a la racine du git
* Appuyer sur flasher
* Attendre le temps de l'écriture de la memoire
* Se connecter au réseau wifi de l'ESP
* Renseigner le réseau wifi 
* Une fois le réseau wifi configuré, la console de ESP-Flasher nous donne l'ip.
* Se connecter sur l'ip et vérifier que la caméra fonctionne

### Configurer la caméra sur motioneye ###

* Se connecter à l'adresse 192.168.20.99:8765
* Les credentials sont les suivants: admin et pas de mot de passe.
* Cliquer sur ajouter une caméra
* Selectionner CaméraType -> Network Camera
* URl: http://<ip-esp>:81/cam.mjpeg
* Appuyer sur entrer
* La caméra devrait apparaitre	 
